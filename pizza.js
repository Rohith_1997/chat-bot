
'use strict';

let Wit = null;
let interactive = null;
try {
  // if running from repo
  Wit = require('../').Wit;
  interactive = require('../').interactive;
} catch (e) {
  Wit = require('node-wit').Wit;
  interactive = require('node-wit').interactive;
}
//var count=0;
const accessToken = (() => {
  if (process.argv.length !== 3) {
    console.log('usage: node examples/quickstart.js <wit-access-token>');
    process.exit(1);
  }
  return process.argv[2];
})();

// Quickstart example
// See https://wit.ai/ar7hur/quickstart


const actions = {
  send(request, response) {
    const {sessionId, context, entities} = request;
    const {text, quickreplies} = response;
    console.log('reply from bot...', JSON.stringify(response));
  },
  pizza_pre({context, entities}) {
    return new Promise(expo.pizza_pre);
  },
  pizza({context, entities}) {
    return new Promise(expo.pizza);
  },
  // rt_pre({context, entities}) {
  //   return new Promise(function (resolve, reject) {
  //
  //     if (context.newData) {
  //       entities = context.newData;
  //       delete context.newData;
  //     }
  //
  //     console.log('rt_pre');
  //     console.log('context in: ', JSON.stringify(context));
  //     console.log('entities in: ', JSON.stringify(entities));
  //
  //     var datetime = firstEntityValue(entities, 'datetime');
  //     var number = firstEntityValue(entities, 'number');
  //
  //     if ( datetime )
  //     { context.date_str = datetime; }
  //
  //     if ( context.date_str )
  //     { delete context.missingDate; }
  //     else
  //     { context.missingDate = true; }
  //
  //     if ( number )
  //     { context.number_str = number; }
  //
  //     if ( context.number_str )
  //     { delete context.missingNum; }
  //     else
  //     { context.missingNum = true; }
  //
  //     console.log('context out: ', JSON.stringify(context));
  //
  //     return resolve(context);
  //   });
  // },
  // rt({context, entities}) {
  //   return new Promise(function (resolve, reject) {
  //
  //     console.log('rt');
  //     console.log('context in: ', JSON.stringify(context));
  //     console.log('entities in: ', JSON.stringify(entities));
  //
  //     var datetime = firstEntityValue(entities, 'datetime');
  //     var number = firstEntityValue(entities, 'number');
  //     var yes_no = firstEntityValue(entities, 'yes_no');
  //
  //     if (datetime || number) {
  //       context.newData = entities;
  //     }
  //     else {
  //       if (context.date_str
  //         && context.number_str
  //         && (yes_no === 'yes'))
  //       { context.rt_success = 'Ok, the table is waiting for you.'; }
  //       else
  //       { context.rt_success = 'Sorry, forget it.'; }
  //
  //       delete context.date_str;
  //       delete context.number_str;
  //     }
  //
  //     console.log('context out is: ', JSON.stringify(context));
  //
  //     return resolve(context);
  //   });
  // },


};

const client = new Wit({accessToken, actions});
interactive(client);
