var pizza =require('./methods/pizza_expo');
var child_process = require('child_process');
var weather=require('./methods/weather');
var greetings=require('./methods/trueGreetings');
const request = require('request');
var rp = require('request-promise');
var child_process = require('child_process');

'use strict';

let Wit = null;
let interactive = null;
try {
  Wit = require('../').Wit;
  interactive = require('../').interactive;
} catch (e) {
  Wit = require('node-wit').Wit;
  interactive = require('node-wit').interactive;
}

const accessToken = (() => {
  if (process.argv.length !== 3) {
    console.log('usage: node examples/quickstart.js <wit-access-token>');
    process.exit(1);
  }
  return process.argv[2];
})();

// Quickstart example
// See https://wit.ai/ar7hur/quickstart
const actions = {
  send(request, response) {
    const {sessionId, context, entities} = request;
    const {text, quickreplies} = response;
    console.log('reply from bot...', JSON.stringify(response));
  },

  trueGreetings({context,entities}) {
    greetings.trueGreetings({context,entities});
    return context;
  },
  getForecast({context, entities}) {
    weather.getForecast({context,entities});
    return context;
  },
  pizza_pre({context, entities}) {
      return pizza.pizza_pre({context,entities});
  },
  pizza({context, entities}) {
    return pizza.pizza({context,entities});
  },
};

const client = new Wit({accessToken, actions});
interactive(client);
